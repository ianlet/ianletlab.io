---
layout: post
title:  "A new beginning"
date:   2018-09-21 17:37:14 -0500
categories: [welcome, first post, software engineering, tdd, atdd, bdd, tests, testing, learning, teaching, travelling, exploring]
---

It has been a long time since I started thinking about having my own website with a few blog posts on subjects I am
interested in. As I am always looking for new opportunities to learn and to help others, it sounded like a good way
for me to acheive that. 

## What you will find here

I am mostly interested in software engineering, complex system solving, clean code, DDD, BDD, automated tests, TDD, ATDD and
testing in general. In addition to technical skills, I am also looking to improve some of my soft skills as this is a
very important aspect of software development. You will particularly find posts about these subjects.

But I am also travelling, backpacking here and there and enjoying the wilderness. So it may be possible I write some
articles about that too, post a few pictures and videos I made.
