![Build Status](https://gitlab.com/ianlet/ianlet.gitlab.io/badges/master/build.svg) ![Jekyll Version](https://img.shields.io/gem/v/jekyll.svg)

## Building locally

To work locally with this project, you'll have to follow the steps below:

1. Fork, clone or download this project
1. [Install][] Jekyll
1. Generate the website: `jekyll build -d public`
1. Preview your project: `jekyll serve`
1. Add content

Read more at Jekyll's [documentation][].

[Jekyll]: http://jekyllrb.com/
[install]: https://jekyllrb.com/docs/installation/
[documentation]: https://jekyllrb.com/docs/home/
