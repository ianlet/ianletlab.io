---
layout: post
title:  "Tell Me What to Do, but Don't Envy My Data nor My Features"
categories: [code smell, development practices, clean code, solid, tda, feature envy, data class, data envy]
---

The [Tell Don't Ask][tda] (TDA) principle has been known for quite a long time now and is usually a way to fix the code smells
[Data Envy][data-envy] and [Feature Envy][feature-envy]. When introduced to the TDA for the first time, some developers
try to clumsily respect it by implementing a solution that usually fixes Data Envy but keeps Envying Features of another
class.

[tda]: https://www.martinfowler.com/bliki/TellDontAsk.html
[feature-envy]: http://wiki.c2.com/?FeatureEnvySmell
[data-envy]: http://wiki.c2.com/?DataEnvy

## What are Data Envy & Feature Envy?

An interesting [definition][define-envy] of the word "envy" is the feeling of discontent and resentment aroused by and
in conjunction with desire for the possessions or qualities of another. I know that code does not have feelings, but the
important part here is the `desire for the possessions or qualities of another`. In the context of object-oriented
programming, the terms Data Envy and Feature Envy came up to describe that desire for the possessions (data) or the
qualities (features) of another object. In other words, it means that a method interacts more with the data or the
methods of another class that it does with its own.

In some rare cases, that could be perfectly fine. As always, it depends on the context. But usually, several problems
can occur if we face one of those smells, such as a stronger coupling between classes or knowledge duplication.

Let's have an example:

{% highlight javascript linenos %}
class Investor {
  buyStock(stock, quantity) {
    const total = stock.unitPrice * quantity + this.account.txFees

    if (this.account.balance < total) {
      throw new Error("You're too poor to buy that stock, sorry")
    }
    
    this.account.balance -= total
    this.stocks[stock.symbol] += quantity
  }
}
{% endhighlight %}

The behavior here is quite straightforward. The investor calculates the total price to buy a stock, then it checks
the account balance to decide if there is enough money to buy that stock and finally it withdraws the money from the
account and increments the quantity of stocks it posesses. Quite a lot of responsibilities for an investor!

So, can you spot the problems here?

1. The `Investor` envies data from the `stock` (its price) and from the `account` (the transaction fees and its balance)
2. It knows how to calculate the total price to buy that stock
3. It decides if the account has enough money
4. It changes the balance of the account by itself

Let's say we ignore those problems for now and that we add another methods to sell a stock that an investor owns:

{% highlight javascript linenos %}
class Investor {
  // ...

  sellStock(stock, quantity) {
    const total = stock.unitPrice * quantity - this.account.txFees

    if (total < 0 && this.account.balance < abs(total)) {
      throw new Error("Oops, you cannot pay the transactions fees even after selling that stock")
    }

    if (this.stocks[stock.symbol] < quantity) {
      throw new Error("You have to own that stock in sufficient quantity to sell it")
    }

    this.account.balance += total
    this.stocks[stock.symbol] -= quantity
  }
}
{% endhighlight %}

The code is quite similar, but we added yet another problem! The knowledge to decide if the account has enough money
is now duplicated in two different places in the code. So now, if your client asks you for example that no account
can have a balance of zero, you'll end up having to remember to change both places.


[define-envy]: https://www.wordnik.com/words/envy
[data-class]: hi

## Tell Don't Ask to the rescue


