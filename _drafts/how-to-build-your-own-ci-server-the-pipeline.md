---
layout: post
title: "How to Build Your Own CI Server - The Pipeline"
categories: [continuous integration, CI, server, pipeline, software, software development, development practices]
---

>>>> FIXME: Maybe a banner to illustrate the article?

> This article is part of a series on How to Build Your Own CI Server where we will step by step discover the
> parts required to build our CI Server.
> - [The Pipeline]() _(you're reading it right now)_
> - [The Runner]()
> - [The Server]()
> - [Make it scale]()

While assisting a teacher in its class on software engineering and software quality, we had the idea to simulate a
real production environment so our students can submit their assignments and have real time feedback on what they were
building. No more suspens until the submission day (or actually a couple of days later) to know whether they failed
or passed, how cool is that!

But to be honest, this wasn't the main reason why we wanted to do that. This was rather a nice side effect. The real
goal behind this initiative was to automate most of our job, as assistants. That way, we can do our best to focus on
the quality of the software students build and give them better feedback instead of running after futile bugs and non
compliant spec.

And that's how we ended up building our own continuous integration (CI) server. We couldn't find an open source and
flexible enough platform to accomodate our needs and to simulate a production environment for the students at the same
time. It was an enriching experience that I will try to generalize here in this series on How to Build Your Own CI
Server.

# Continuous Integration

Nowadays, people talk about continuous integration as a tool, or as a server, that runs jobs on a piece of code and
maybe deploys it to a staging environment or to production (as in [continuous deployment][cd-wiki]), if everything goes
well. But it is way more than that.

Continuous Integration is a software development practice that allows us to continuously integrate with confidence small
pieces of working software within our product. It builds quality in and enforces characteristics we (or more precisely
our customers) want for our product in each and every commit. It gives us rapid and frequent feedback so we can perform
better. It's a living thing that needs to change and adapt as we discover new stuff about our product. So one does not
simply describe a continuous integration pipeline at the beginning of our project and never adapt it afterwards.

[cd-wiki]: https://en.wikipedia.org/wiki/Continuous_deployment

# The Anatomy of a Pipeline

>>>> FIXME: insert image here

In its most simple form, a continuous integration pipeline is a series of tests we run against our code to make sure the
changes we just made will fit into our product. It means that we can put whatever we want in our pipeline, as long as it
informs us what went wrong when it fails. To support this, our CI Server should provide a flexible way to define a
pipeline with as many steps as we need. In other words, it should be easily configurable. And this will be the first
ingredient for our pipeline:

```
The Pipeline should be:
- Configurable
```

