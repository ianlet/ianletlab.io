---
layout: post
title: "Your Architecture is Fat"
categories: [architecture, development practices, patterns, design]
---

Why -> too expensive to build, operate, maintain or change
What -> architecture on a diet
How -> taking good habits

---

Example
Point
Example

or

Point
Example
Point

---

State the problem

---

It's easy to get lost in a sea of new tools, new practices, new architectures


Your architecture is fat, but so is mine.

An architecture is considered fat as soon as it is too expensive to build, operate, maintain or change
for a specific problem.

The simpler the problem, the simpler the architecture should be.


- fat architectures
- bad habits
- taking good habits
- maintaining good habits

