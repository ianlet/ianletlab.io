---
layout: post
title:  "Applying Domain-driven design (DDD) through Command-query responsibility segragation (CQRS) and Event-sourcing (ES) - Introduction"
date:   2016-12-26 20:08:14 -0500
categories: [series, software architecture, ddd, cqrs, es, rust]
---

This marks the beginning of a probably quite long series on how to apply [Domain-driven design][ddd-wiki]{:target="_blank"} (DDD) through [Command-query responsibility segragation][cqrs-wiki]{:target="_blank"}
(CQRS) and [Event-sourcing][es-wiki]{:target="_blank"} (ES).

I've been particularly interested by these different subjects since quite a long time but I've never really had the opportunity to experiment with them in a real life project. So I've decided to start a side
project to correctly learn how to use CQRS and ES with respect of the Domain-driven design philosophy.

I know there are already [many interesting resources][ddd-awesome]{:target="_blank"} covering these techniques, but I've never been able to find a good example of project, with a complex domain, showing how to
correctly implement them. Something different than a blog, an online-store or the [cargo example][ddd-sample]{:target="_blank"}. So almost every step of the development will be described here. That way,
you can join me in my journey to master these subjects.

I've decided to implement the project with the [Rust][rust-lang]{:target="_blank"} language. I think this is a surprisingly good and very powerful language, with a great community. I was seduced by its ownership
system that allows you to build safe and fast applications (at a given cost, obviously) and I am pretty sure it is very suitable for an application implementing CQRS and ES.

## The project

Currently, I am 5km away from my job. I have no car and the bus system of my city is so crappy it's faster to walk to get there. But now the winter came (I braced myself, don't worry) and walking at -25°C is not
the funniest thing to do. So sometimes, I take a ride with Uber. The problem is that it costs ~15$CAD, so ~30$CAD a day. [UberPOOL][uber-pool] is not available here, and probably won't be before a long time,
so it made me think about the challenges of making a similar application. Just the application itself, not what you have to do to make a real product and sell it.

As I was looking for a project to learn DDD, CQRS and ES, implementing the core features of a car pooling application seemed to be an interesting challenge. But for the sake of the project, I don't want to loose
too much time on the frontend of the project. So I will focus on the domain itself and expose it by a thin REST API.

[ddd-wiki]: https://en.wikipedia.org/wiki/Domain-driven_design
[cqrs-wiki]: https://en.wikipedia.org/wiki/Command%E2%80%93query_separation#Command_Query_Responsibility_Segregation
[es-wiki]: http://www.martinfowler.com/eaaDev/EventSourcing.html
[ddd-awesome]: https://github.com/heynickc/awesome-ddd
[ddd-sample]: https://github.com/citerus/dddsample-core
[rest-wiki]: https://en.wikipedia.org/wiki/Representational_state_transfer
[api-wiki]: https://en.wikipedia.org/wiki/Application_programming_interface 
[rust-lang]: https://www.rust-lang.org/en-US/ 
[uber-pool]: https://newsroom.uber.com/effortlesspool/
